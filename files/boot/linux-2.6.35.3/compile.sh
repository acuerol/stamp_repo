#!/bin/bash

echo "Copying ok.config to .config..."
make clean
cp ok.config .config
make ARCH=arm CROSS_COMPILE="../buildroot-2015.05/output/host/usr/bin/arm-acuerol-linux-uclibcgnueabi-" oldconfig -j6
make ARCH=arm CROSS_COMPILE="../buildroot-2015.05/output/host/usr/bin/arm-acuerol-linux-uclibcgnueabi-" -j6

make ARCH=arm CROSS_COMPILE="../buildroot-2015.05/output/host/usr/bin/arm-acuerol-linux-uclibcgnueabi-" modules -j6
