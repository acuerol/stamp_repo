################################################################################
#
# xc3sprog
#
################################################################################

XC3SPROG_VERSION = 760
XC3SPROG_SITE = https://svn.code.sf.net/p/xc3sprog/code/trunk
XC3SPROG_SITE_METHOD = svn
X3CSPROG_LICENSE = GPLv2+
X3CSPROG_LICENSE_FILES = COPYING
X3CSPROG_DEPENDENCIES = libftdi

XC3SPROG_CONF_OPTS += \
	-DVERSION_STRING=0.0-svn$(XC3SPROG_VERSION) \
	-DCMAKE_EXE_LINKER_FLAGS=-lpthread

$(eval $(cmake-package))
$(eval $(host-cmake-package))
